require 'rails_helper'

describe ExchangeRate do

  subject(:model) { ExchangeRate.new }

  describe 'API' do
    it { is_expected.to respond_to :date }
    it { is_expected.to respond_to :date_formated }
    it { is_expected.to respond_to :value }
  end

end
