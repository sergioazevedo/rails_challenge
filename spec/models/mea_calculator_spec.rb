require 'rails_helper'

describe MeaCalculator do

  subject { MeaCalculator.new }

  describe 'API' do
    it { is_expected.to respond_to :calculate }
    it { is_expected.to respond_to :calculate_ema_factor }
  end

  describe "#calculate_ema_factor" do
    it "return the factor to be used in EMA calculus" do
       expect( subject.calculate_ema_factor(5) ).to be_within(0.01).of(0.33)
    end
  end

  describe "#calculate_ema" do
    it "return 16.3 as result to ema calculation where today_price = 17, yesterday_ema = 16 and ema_factor is 0.33" do
      result = subject.calculate_ema(today_price: 17, yesterday_ema: 16, ema_factor: 0.33)
      expect(result).to be_within(0.05).of(16.3)
    end

    it "return 14.4 as result to ema calculation where today_price = 10, yesterday_ema = 16.5 and ema_factor is 0.33" do
      result = subject.calculate_ema(today_price: 10, yesterday_ema: 16.5, ema_factor: 0.33)
      expect(result).to be_within(0.05).of(14.4)
    end

    it "return 16 as result to ema calculation where today_price = 16, yesterday_ema = 0 and ema_factor is 0.33" do
      result = subject.calculate_ema(today_price: 16, yesterday_ema: 0, ema_factor: 0.33)
      expect(result).to be_within(0.05).of(16)
    end

  end

  describe '#calculate' do
    it "return ArgumentError if temporal_series is missing"  do
      expect{ subject.calculate }.to raise_error
    end

    it "return ArgumentError if days is missing"  do
      expect{ subject.calculate(temporal_series: []) }.to raise_error
    end

    context "given a valid temporal_series and the 5 days time period" do
      let(:rates){[16,17,17,10,17,18,17,17,17]}
      let(:temporal_series) do
        list = []
        rates.each_with_index do |value, idx|
          list << ExchangeRate.new(date: Date.today + idx, value: value)
        end

        list
      end

      it "returns the correct MEA for each value" do
        result = subject.calculate(temporal_series: temporal_series, days: 5)
        expect(result).to match_array [16.0, 16.33, 16.56, 14.37, 15.25, 16.16, 16.44, 16.63, 16.75]
      end
    end

  end

end
