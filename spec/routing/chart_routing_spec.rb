require 'rails_helper'

describe ChartController, type: :routing do

  describe "routing" do
    it { expect( get: "/").to route_to("chart#index") }
    it { expect( get: "/chart").to route_to("chart#show") }
    it { expect( post: "/chart").not_to be_routable }
    it { expect( put: "/chart").not_to be_routable }
    it { expect( patch: "/chart").not_to be_routable }
    it { expect( delete: "/chart").to_not be_routable }
  end

end