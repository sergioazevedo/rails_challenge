require 'rails_helper'

feature "user choose a currency in the selection box", :type => :feature do

  scenario "user selects EUR on the select box " do
    visit root_path
    select 'EUR', from: 'Currency'
    expect(page).to have_content('EUR')
  end

  scenario "user selects BRL on the select box " do
    visit root_path
    select 'BRL', from: 'Currency'
    expect(page).to have_content('BRL')
  end

end
