require 'rails_helper'

feature "User visualizes a chart of Exchange Rates", :type => :feature do

  scenario "In the first look of application, user will see the last month rates of: USDxBRL", js: true do
    visit root_path
    select 'BRL', from: 'Currency'
    click_button "Generate Chart!"
    expect(page).to have_css('.highcharts-container')
  end

end
