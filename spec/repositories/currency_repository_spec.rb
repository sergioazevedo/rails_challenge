require 'rails_helper'

describe CurrencyRepository do

  subject(:repository) { CurrencyRepository.new }

  describe 'API' do
    it { is_expected.to respond_to :retrieve_available_currencies }
  end

  describe "#retrieve_available_currencies" do

    it "returns a collection" do
      expect(repository.retrieve_available_currencies).to respond_to(:each)
    end

    context "when the communication fails" do
      before do
        allow(repository).to receive(:open){ raise SocketError.new }
      end
      it "returns a empty collection" do
        expect(repository.retrieve_available_currencies).to be_empty
      end
    end

  end

end
