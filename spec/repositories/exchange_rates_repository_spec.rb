require 'rails_helper'

describe ExchangeRatesRepository do

  subject(:repository) { ExchangeRatesRepository.new }

  describe 'API' do
    it { is_expected.to respond_to :retrieve_last_month_rates }
    it { is_expected.to respond_to :retrieve_rates }
  end

  describe "#retrieve_last_month_rates" do
    it "returns a collection" do
      expect(repository.retrieve_last_month_rates(to: "BRL")).to respond_to(:each)
    end

    it "returns elements that responds to date" do
      list = repository.retrieve_last_month_rates(to: "BRL")
      expect(list.first).to respond_to(:date)
    end

    it "returns elements that responds to value" do
      list = repository.retrieve_last_month_rates(to: "BRL")
      expect(list.first).to respond_to(:value)
    end

    context "when the communication fails" do
      before do
        allow(repository).to receive(:open){ raise SocketError.new }
      end
      it "returns a empty collection" do
        expect(repository.retrieve_last_month_rates(to: "BRL")).to be_empty
      end
    end
  end

  describe "#retrieve_rates" do
    let(:start_date){1.year.ago.to_date}
    let(:end_date){Date.today}
    it "returns a collection" do
      expect(repository.retrieve_rates(to: "BRL", start_date: start_date, end_date: end_date)).to respond_to(:each)
    end

    it "returns elements that responds to date" do
      list = repository.retrieve_rates(to: "BRL", start_date: start_date, end_date: end_date)
      expect(list.first).to respond_to(:date)
    end

    it "returns elements that responds to value" do
      list = repository.retrieve_rates(to: "BRL", start_date: start_date, end_date: end_date)
      expect(list.first).to respond_to(:value)
    end

    it "returns first rate date is equal to start_date" do
      list = repository.retrieve_rates(to: "BRL", start_date: start_date, end_date: end_date)
      expect(list.first.date).to eq start_date
    end

    context "when the communication fails" do
      before do
        allow(repository).to receive(:open){ raise SocketError.new }
      end
      it "returns a empty collection" do
        expect(repository.retrieve_rates(to: "BRL", start_date: start_date, end_date: end_date)).to be_empty
      end
    end
  end
end
