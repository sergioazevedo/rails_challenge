class ExchangeRate
  include ActiveAttr::Model

  attribute :date, type: Date
  attribute :value, type: Float


  def date_formated(format: "%m/%d/%y")
    date.strftime(format)
  end

end