class MeaCalculator

  def calculate(temporal_series: nil, days: nil)
    raise ArgumentError.new unless temporal_series && days
    ema_factor = calculate_ema_factor(days)
    yesterday_ema = 0

    temporal_series.map do |exchange_rate|
      new_ema = calculate_ema(
        today_price: exchange_rate.value,
        yesterday_ema: yesterday_ema,
        ema_factor: ema_factor
      )
      yesterday_ema = new_ema

      new_ema.round(2)
    end

  end

  def calculate_ema(today_price: nil, yesterday_ema: nil, ema_factor: nil)
    if yesterday_ema.to_i > 0
      (today_price * ema_factor) + (yesterday_ema * (1 - ema_factor))
    else
      today_price
    end
  end

  def calculate_ema_factor(days)
    2.0/(days + 1)
  end


end