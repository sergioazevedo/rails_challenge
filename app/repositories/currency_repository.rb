class CurrencyRepository

  def retrieve_available_currencies
    begin
      rates_hash = get_currency_data_from_remote

      rates_hash.keys
    rescue => e
      []
    end
  end

  private

  def get_currency_data_from_remote
    stream = open("http://jsonrates.com/get/?base=USD")
    data = stream.read
    json = JSON.parse(data)

    json["rates"]
  end

end