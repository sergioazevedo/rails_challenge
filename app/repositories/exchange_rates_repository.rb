class ExchangeRatesRepository

  def retrieve_last_month_rates(from: "USD", to: nil)
    raise ArgumentError.new unless to
    start_date = 1.month.ago.to_date
    end_date = Date.today
    retrieve_rates(from: from, to: to, start_date: start_date, end_date: end_date)
  end

  def retrieve_rates(from: "USD", to: nil, start_date: nil, end_date: nil)
    raise ArgumentError.new unless to
    url = build_url(from, to, start_date, end_date)
    begin
      data = get_data_from_remote(url)
      build_exchange_rate_list(data)
    rescue
      []
    end
  end

  private

  def build_url(from, to, start_date, end_date)
    "http://jsonrates.com/historical/?from=#{from}&to=#{to}&dateStart=#{start_date}&dateEnd=#{end_date}"
  end

  def get_data_from_remote(url)
    stream = open(url)
    data = stream.read
    json = JSON.parse(data)
    json["rates"]
  end

  def build_exchange_rate_list(data)
    data.map do |k,v|
      ExchangeRate.new(date: k.to_date, value: v["rate"].to_f)
    end
  end

end