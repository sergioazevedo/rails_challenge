class ChartController < ApplicationController
  before_filter :basic_setup

  def index
  end

  def show
    currency = params[:currency]
    temporal_series = ExchangeRatesRepository.new.retrieve_rates(
      from:'USD',
      to: currency,
      start_date: @start_date,
      end_date: @end_date,
    )
    @chart = generate_chart_from(temporal_series)
  end

  private

  def basic_setup
    @avaiable_currencies = retrieve_currencies_for_options_to_select
    @start_date = params[:start_date] || 1.month.ago.to_date.strftime("%Y-%m-%d")
    @end_date = params[:end_date] || Date.today.strftime("%Y-%m-%d")
  end

  def retrieve_currencies_for_options_to_select
    data = CurrencyRepository.new.retrieve_available_currencies
    data.map{|value| [value,value] }
  end


  def generate_chart_from(temporal_series)
    LazyHighCharts::HighChart.new('graph') do |c|
      c.title text: "USDx#{params[:currency]}"
      c.options[:xAxis][:categories] = extract_categories_from(temporal_series)
      c.series type: 'line',
               name: "USDx#{params[:currency]}",
               data: extract_rates_from( temporal_series )
      c.series type: 'spline',
               name: "MEA",
               data: calculate_mea_from( temporal_series )
    end
  end

  def extract_categories_from( temporal_series )
    temporal_series.map(&:date_formated)
  end

  def extract_rates_from( temporal_series )
    temporal_series.map(&:value)
  end

  def calculate_mea_from( temporal_series )
    MeaCalculator.new.calculate(temporal_series: temporal_series, days: 21)
  end

end
